# libnetgsm
# Copyright (C) 2019  Arda "Ave" Ozkal

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests


def get_sms_headers(usercode, password):
    url = ("https://api.netgsm.com.tr/sms/header/get/?usercode={}&password={}"
           .format(usercode, password))
    req = requests.get(url)

    # TODO: Check for error codes and return proper response

    return req.text.strip("<br>").split("<br>")


def get_account_balance(usercode, password):
    url = ("https://api.netgsm.com.tr/balance/list/get/?usercode={}&password={}"
           .format(usercode, password))
    req = requests.get(url)

    return req.text

    values = req.text.split(" ")

    if values[0] == "00":
        return values[1]
    elif values[0] == "30":
        return [False, "Geçersiz kullanıcı adı, şifre veya kullanıcınızın API erişim izninin olmadığını gösterir. Ayrıca eğer API erişiminizde IP sınırlaması yaptıysanız ve sınırladığınız ip dışında gönderim sağlıyorsanız bu hata kodunu alırsınız. API erişim izninizi veya IP sınırlamanızı, web arayüzünden; sağ üst köşede bulunan ayarlar > API işlemleri menüsunden kontrol edebilirsiniz."]
    elif values[0] == "40":
        return [False, "Arama kriterlerinize göre listelenecek kayıt olmadığını ifade eder."]
    elif values[0] == "100":
        return [False, "System error or ratelimited. (limit is 5 times/minute)"]


def get_pack_offer_balance(usercode, password):
    url = ("https://api.netgsm.com.tr/balance/list/get/"
           "?usercode={}&password={}&tip=1"
           .format(usercode, password))
    req = requests.get(url)

    values = req.text.strip("<BR>").split("<BR>")

    if values[0] == "00":
        return values[1]
    elif values[0] == "30":
        return [False, "Geçersiz kullanıcı adı, şifre veya kullanıcınızın API erişim izninin olmadığını gösterir. Ayrıca eğer API erişiminizde IP sınırlaması yaptıysanız ve sınırladığınız ip dışında gönderim sağlıyorsanız bu hata kodunu alırsınız. API erişim izninizi veya IP sınırlamanızı, web arayüzünden; sağ üst köşede bulunan ayarlar > API işlemleri menüsunden kontrol edebilirsiniz."]
    elif values[0] == "40":
        return [False, "Arama kriterlerinize göre listelenecek kayıt olmadığını ifade eder."]
    elif values[0] == "100":
        return [False, "System error or ratelimited. (limit is 5 times/minute)"]
