## libNetGSM

Unofficial NetGSM library for Python. 

NetGSM is a Turkish online GSM operator. They provide SMS APIs and other similar telephony things, similar to Twilio, except much cheaper (and much harder to register to). They provide an API that is not... very modern, let's say. This library tries to change that.

One of the projects that I plan to build with this project is a middleware that'll provide a RESTful JSON API for interacting with NetGSM, so look forward to that.

### Disclaimer

First off, this is an unofficial library.

This is a pre-release, WIP library and is far from done. Only reason I'm making this public is because another project of mine relies on this and I want to git submodule it and have it be cloneable when I release that.

If you don't want your code to break: Don't rely on returned information. Don't rely on arguments or function names staying static.

SMS sending stuff will likely have same function name and arguments, but the rest may change.

### Features implemented

- Ability to send a 1:n SMS (`send_sms(usercode, password, msgheader, msg, recipient_numbers, startdate="", stopdate="", turkish=None)`, the Turkish field determines Turkish support, if set to None it's automatically detected if message contains Turkish characters or not, and can be overriden with `True` or `False`)
- Ability to send an n:n SMS (`send_sms_nn(usercode, password, msgheader, number_msgs, startdate="", stopdate="", turkish=True)`)
- Ability to send an OTP SMS (`send_sms_otp(usercode, password, msgheader, msg, recipient_numbers, startdate="", stopdate="")` -- untested, I don't have a OTP plan)

- Ability to get character count of a message, with Turkish characters accounted for (`charlen(message)`)
- Ability to get SMS count of a message (`messagecount(message, named=False)`)
- Ability to query SMS headers a user can use (`get_sms_headers(usercode, password)`)
- Ability to query account balance (`get_account_balance(usercode, password)`)
- Ability to query offer balance (`get_pack_offer_balance(usercode, password)`)

### Features to be implemented

- Ability to query waiting tasks
- Ability to query previous tasks (SMS)
- "Report" feature

Also I need to add some doctexts, some more documentation, some examples, properly read error and success codes and return an actually readable text instead of undecipherable numbers.

### License

This software is licensed AGPLv3, and a copy is provided on the LICENSE file.

Violations of the license without written approval beforehand from me will not be tolerated.

If you want an exemption from this license, email me at `ave` at `lasagna dot dev`.
