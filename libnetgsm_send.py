# libnetgsm
# Copyright (C) 2019  Arda "Ave" Ozkal

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lxml import etree
import requests
import libnetgsm.libnetgsm_calc as libnetgsm_calc


def process_message(msg):
    new_msg = msg.replace("\n", "\\n")
    return new_msg


def prep_sms_xml(usercode, password, msgheader, msg_type,
                 startdate="", stopdate="", turkish=False):
    # Form XML
    mainbody = etree.Element('mainbody')
    header = etree.Element('header')
    mainbody.append(header)

    # Add usercode
    child = etree.Element('usercode')
    child.text = str(usercode)
    header.append(child)

    # Add password
    child = etree.Element('password')
    child.text = password
    header.append(child)

    if msg_type != "otp":
        # Add company field
        child = etree.Element('company')

        # Enable support for Turkish characters if requested (and if not OTP)
        if turkish and msg_type != "otp":
            child.set("dil", "TR")

        child.text = "Netgsm"
        header.append(child)

        # Add type
        child = etree.Element('type')
        child.text = msg_type
        header.append(child)

    # Add msgheader
    child = etree.Element('msgheader')
    child.text = str(msgheader)
    header.append(child)

    # Add startdate if specified
    # TODO: form this from datetime with ddMMyyyyHHmm
    if startdate and msg_type != "otp":
        child = etree.Element('startdate')
        child.text = startdate
        header.append(child)

    # Add stopdate if specified
    # TODO: form this from datetime with ddMMyyyyHHmm
    if stopdate and msg_type != "otp":
        child = etree.Element('stopdate')
        child.text = stopdate
        header.append(child)

    return mainbody


def send_sms(usercode, password, msgheader, msg,
             recipient_numbers, startdate="", stopdate="",
             turkish=None):

    # Refuse sending messages when it's too long
    if libnetgsm_calc.messagecount(msg, usercode != msgheader) > 9:
        return [False, "Message too long"]

    if turkish is None:
        turkish = libnetgsm_calc.has_turkish_char(msg)

    # Form XML
    mainbody = prep_sms_xml(usercode, password, msgheader,
                            "1:n", startdate, stopdate,
                            turkish=turkish)
    body = etree.Element('body')
    mainbody.append(body)

    # Add msg
    msg = process_message(msg)
    child = etree.Element('msg')
    child.text = etree.CDATA(msg)
    body.append(child)

    # Add recipient numbers
    for recipient in recipient_numbers:
        child = etree.Element('no')
        child.text = str(recipient)
        body.append(child)

    xmlresult = etree.tostring(mainbody).decode()

    req = requests.post("https://api.netgsm.com.tr/sms/send/xml",
                        data=xmlresult)

    # TODO: Check for error codes and return proper response
    return [req.text[0:2] == "00", req.text]


def send_sms_nn(usercode, password, msgheader,
                number_msgs, startdate="", stopdate="",
                turkish=True):
    # Form XML
    mainbody = prep_sms_xml(usercode, password, msgheader,
                            "n:n", startdate, stopdate,
                            turkish=turkish)
    body = etree.Element('body')
    mainbody.append(body)

    # Add message objects
    for recipient in number_msgs:
        mp = etree.Element('mp')
        body.append(mp)

        msg = number_msgs[recipient]

        # Refuse sending messages when it's too long
        if libnetgsm_calc.messagecount(msg, usercode != msgheader) > 9:
            return [False, "Message to {} is too long".format(recipient)]

        # Add msg
        msg = process_message(msg)
        child = etree.Element('msg')
        child.text = etree.CDATA(msg)
        mp.append(child)

        # Add number
        child = etree.Element('no')
        child.text = str(recipient)
        mp.append(child)

    xmlresult = etree.tostring(mainbody).decode()

    req = requests.post("https://api.netgsm.com.tr/sms/send/xml",
                        data=xmlresult)

    # TODO: Check for error codes and return proper response

    return [req.text[0:2] == "00", req.text]


def send_sms_otp(usercode, password, msgheader, msg,
                 recipient_numbers, startdate="", stopdate=""):
    # Refuse sending messages when it's too long
    if libnetgsm_calc.messagecount(msg, usercode != msgheader) > 9:
        return [False, "Message too long"]

    # Form XML
    mainbody = prep_sms_xml(usercode, password, msgheader,
                            "otp", startdate, stopdate)
    body = etree.Element('body')
    mainbody.append(body)

    # Add msg
    msg = process_message(msg)
    child = etree.Element('msg')
    child.text = etree.CDATA(msg)
    body.append(child)

    # Add recipient numbers
    for recipient in recipient_numbers:
        child = etree.Element('no')
        child.text = str(recipient)
        body.append(child)

    xmlresult = etree.tostring(mainbody).decode()

    req = requests.post("https://api.netgsm.com.tr/sms/send/otp",
                        data=xmlresult)

    # TODO: Check for error codes and return proper response

    return req.text
