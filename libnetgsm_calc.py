# libnetgsm
# Copyright (C) 2019  Arda "Ave" Ozkal

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math

turkish_chars = ["ç", "ğ", "ı", "ş", "Ğ", "İ", "Ş"]


def has_turkish_char(message):
    for char in turkish_chars:
        if char in message:
            return True
    return False


def charlen(message):
    for char in turkish_chars:
        # HACK: Make Turkish characters (stated above) count as 2
        message = message.replace(char, char * 2)
    return len(message)


def messagecount(message, named=False):
    message_size = 160
    charcount = charlen(message)

    # HACK: 5 extra characters are used up when sending SMSes with Turkish chars
    if charcount != len(message):
        charcount += 5
    # Due to the " B021", message size is increased by 5 on named SMSes
    if named:
        charcount += 5

    return math.ceil(charcount / message_size)
